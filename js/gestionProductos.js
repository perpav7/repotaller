var productosObtenidos;
var clientesObtenidos;

function getProductos(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos(){
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i = 0; i < JSONProductos.value.length; i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var ColumnaNombre = document.createElement("td");
    ColumnaNombre.innerText = JSONProductos.value[i].ProductName;
    var ColumnaPrecio = document.createElement("td");
    ColumnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
    var ColumnaStock = document.createElement("td");
    ColumnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(ColumnaNombre);
    nuevaFila.appendChild(ColumnaPrecio);
    nuevaFila.appendChild(ColumnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}

function getClientes(){
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes = JSON.parse(clientesObtenidos);
  var divTabla = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var i = 0; i < JSONClientes.value.length; i++){
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var ColumnaNombre = document.createElement("td");
    ColumnaNombre.innerText = JSONClientes.value[i].ContactName;
    var ColumnaCiudad = document.createElement("td");
    ColumnaCiudad.innerText = JSONClientes.value[i].City;
    var ColumnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if(JSONClientes.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
    }else {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }
    ColumnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(ColumnaNombre);
    nuevaFila.appendChild(ColumnaCiudad);
    nuevaFila.appendChild(ColumnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

}
